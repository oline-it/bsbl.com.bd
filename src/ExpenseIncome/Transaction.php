<?php
namespace App\ExpenseIncome;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Transaction extends  DB{
    private $transaction_Type,$account_head;
    private $customer_id,$branch_id;
    private $bank_id;
    private $from_Transaction;
    private $to_Transaction;
    private $transaction_Date;
    private $voucher_No;
    private $transaction_For;
    private $transaction_Mode;
    private $received_To;
    private $received_From;
    private $cheque_No;
    private $cheque_Date;
    private $remarks;
    private $amount_Total;
    private $in_Words;
    private $modified_Date;
    private $product_id;
    public function setData($postData){


        if(array_key_exists('customerId',$postData)){
            $this->customer_id = $postData['customerId']; }

        if(array_key_exists('partyid',$postData)){
            $this->customer_id = $postData['partyid']; }

        if(array_key_exists('branchid',$postData)){
            $this->branch_id = $postData['branchid']; }
        if(array_key_exists('transactionType',$postData))
        {
            $this->transaction_Type = $postData['transactionType'];
        }
        if(array_key_exists('accheadId',$postData)){
            $this->account_head = $postData['accheadId'];
        }
        if(array_key_exists('bankid',$postData)){
            $this->bank_id = $postData['bankid'];
        }
        if(array_key_exists('particulars',$postData)){
            $this->particulars_Details = $postData['particulars'];}
        if(array_key_exists('salename',$postData)){
            $this->product_id = $postData['salename'];}
        if(array_key_exists('voucherNo',$postData)){
            $this->voucher_No = $postData['voucherNo']; }
        if(array_key_exists('transactionFor',$postData)){
            $this->transaction_For = $postData['transactionFor'];}
        if(array_key_exists('transactionMode',$postData)){
            $this->transaction_Mode = $postData['transactionMode'];}
        if(array_key_exists('receivedTo',$postData)){
            $this->received_To = $postData['receivedTo'];}
        if(array_key_exists('transactionDate',$postData)){
            $this->transaction_Date = $postData['transactionDate'];}

        if(array_key_exists('fromTransaction',$postData)){
            $this->from_Transaction = $postData['fromTransaction'];
        }
        if(array_key_exists('toTransaction',$postData)){
            $this->to_Transaction = $postData['toTransaction'];
        }
        if(array_key_exists('amount',$postData)){
            $this->amount_Total = $postData['amount'];}
        if(array_key_exists('receivedFrom',$postData)){
            $this->received_From = $postData['receivedFrom'];}
        if(array_key_exists('chequeNo',$postData)){
            $this->cheque_No = $postData['chequeNo'];}
        if(array_key_exists('chequeDate',$postData)){
            $this->cheque_Date = $postData['chequeDate'];}
        if(array_key_exists('remarks',$postData)){
            $this->remarks = $postData['remarks'];}
        if(array_key_exists('modifiedDate',$postData)){
            $this->modified_Date = $postData['modifiedDate'];}
        if(array_key_exists('inWords',$postData)){
            $this->in_Words = $postData['inWords'];}
        $this->modified_Date=date('Y-m-d');
        if(empty($this->crvoucher_no))$this->crvoucher_no=NULL;
        if(empty($this->voucher_no))$this->voucher_no=NULL;
        if(empty($this->challan_no))$this->challan_no=NULL;
        if(empty($this->customer_id)||$this->customer_id=='0')$this->customer_id=NULL;
    }
    public function index(){
        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT products.product_name, customers.name, salesentry.id,salesentry.rate,salesentry.bandwidth,salesentry.fromDurationDate,salesentry.toDurationDate,salesentry.totaldays,salesentry.amount,salesentry.receiptInvoiceNo,salesentry.particulars,salesentry.PayerPayee FROM salesentry INNER JOIN products ON salesentry.product_id=products.id INNER JOIN customers ON salesentry.customer_id=customers.id where salesentry.soft_deleted='No'";
         $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function allorders(){

        $sql="SELECT X.id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.soldAmount, X.paidAmount, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) X JOIN ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X.soldAmount !=0 AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X.id";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function individualOrder(){

             $sql="SELECT X.id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.soldAmount, X.paidAmount, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) X JOIN ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE X.soldAmount !=0 AND X.customerId='$this->customer_id' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X .id ";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function allClients(){

        $sql = "select * from customers where soft_deleted='No' ORDER BY partyname ASC";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allparticulars(){

        $sql = "select * from products where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function allbank(){
        $sql = "select * from bank where soft_deleted='No' ORDER BY bankname ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function accounthead(){

        $sql = "select  id, headnameenglish from accounthead where soft_deleted='No' ORDER BY headnameenglish ASC";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function shipexhead(){

        $sql = "select * from shipexpenses where soft_deleted='No' ORDER BY name ASC";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function statement(){

        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='$this->branch_id'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function statementBalance(){

        $sql = "SELECT X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '2017-01-01' AND '2017-12-31' AND X.branchid='$this->branch_id'  GROUP BY X.id ASC;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function bothstatement(){

        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,accounthead.headnamebangla,bank.accountname, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function opneningbalance(){

        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,accounthead.headnamebangla,bank.accountname, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '2018-01-01' AND '2018-05-30' AND X.transactionType='OB' GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function bothStatementBalance(){

        $sql = "SELECT X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '2017-01-01' AND '2017-12-31' GROUP BY X.id ASC;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }
    public function ledger(){
        $sql = "SELECT X.id, X.transactionDate,X.particulars,accounthead.headnameenglish, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='$this->account_head'GROUP BY X.id ORDER BY X.transactionDate ASC, X.voucherNo;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function singleledger(){

           $sql = "SELECT X.id, X.transactionDate,X.particulars,accounthead.headnameenglish, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='$this->branch_id' AND X.accountheadid='$this->account_head'GROUP BY X.id ORDER BY X.transactionDate ASC, X.voucherNo;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function allsalesledger(){

           $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='$this->account_head'GROUP BY X.id ORDER BY X.transactionDate ASC, X.voucherNo ;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function singleBranchallsalesledger(){
        //var_dump($_GET);
           $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='$this->account_head' AND X.branchid='$this->branch_id' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function singleProductSalesledger(){

        $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='$this->account_head' AND X.branchid='$this->branch_id' AND products.id='$this->product_id' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function singleBankSingleBranchledger(){
        $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' AND X.branchid='$this->branch_id' AND X.bankid='$this->bank_id' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function singleBankAllBranchledger(){
        $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' AND X.bankid='$this->bank_id' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function allBankAllBranchledger(){
        $sql = "SELECT X.id, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function bankTrialBalance(){

        $sql = "SELECT X .id, bank.bankname, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' AND X.bankid !=0 GROUP BY bank.id WITH ROLLUP;";
        //$sql = "SELECT X .id, X.transactionDate, X.particulars, products.product_name, accounthead.headnameenglish, bank.bankname, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, X.transactionType, X.bankid, X.voucherNo, X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM salestransaction X LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='59' GROUP BY bank.id WITH ROLLUP;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function partyTrialBalance(){

        //$sql = "SELECT X.id,party.partyname, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname,party.partyname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id LEFT JOIN party ON X.customerid=party.id WHERE X.soft_deleted ='No' AND X.customerid !=0 AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $sql = "SELECT X .id, party.partyname,  SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountIn)-SUM(X.amountOut) AS balance,   X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN sales ON X.challanno = sales.challanno  WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='0' AND X.customerId !=0 GROUP BY X.customerId WITH ROLLUP;";
           $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function saleTrialBalance(){

        $sql = "SELECT X .id,products.product_name, bank.accountname, bank.id AS bankid, SUM(X.amountIn) AS amountIn, SUM(X.amountOut) AS amountOut, SUM(X.amountOut)-SUM(X.amountIn) AS balance,  X.bankid,  X.remarks FROM salestransaction X LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid = accounthead.id LEFT JOIN sales ON X.challanno = sales.challanno LEFT JOIN bank ON X.bankid = bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.accountheadid='24' GROUP BY X.product_id WITH ROLLUP;";
        //$sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,bank.accountname,accounthead.headnamebangla, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND X.branchid='$this->branch_id' GROUP BY  X.id ;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function partyAllledger(){
        $sql = "SELECT X.id,party.partyname, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname,party.partyname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id LEFT JOIN party ON X.customerid=party.id WHERE X.soft_deleted ='No' AND X.customerid !=0 AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X.id ORDER BY X.transactionDate ASC, X.challanno;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function partysingleedger(){
        $sql = "SELECT X.id,party.partyname, X.transactionDate,X.particulars,products.product_name,accounthead.headnameenglish,bank.bankname,party.partyname, sales.tonweight,sales.kgweight, X.amountIn, X.amountOut, SUM(Y.bal) balance, X.transactionType,X.bankid, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN sales ON X.challanno=sales.challanno LEFT JOIN bank ON X.bankid=bank.id LEFT JOIN party ON X.customerid=party.id WHERE X.soft_deleted ='No' AND X.customerId='$this->customer_id' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X.id ORDER BY X.transactionDate, X.challanno ASC;";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    /*public function allStatement(){

             $sql = "SELECT X.id, products.product_name, customers.name, X.rate, X.bandwidth, X.fromDurationDate, X.toDurationDate, X.totaldays, X.receiptInvoiceNo, X.particulars, X.product_id, X.soldAmount, X.paidAmount, SUM(Y.bal) balance, X.transactionType, X.voucherNo, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks, X.customerId FROM ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) X JOIN ( SELECT *, soldAmount - paidAmount bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN customers ON X.customerId = customers.id LEFT JOIN products ON X.product_id = products.id WHERE  X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY X.id";


        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }*/
    public function transaction(){

        $sql = "SELECT x.id ,x.customerId,customers.name, products.product_name,x.remarks,x.particulars,x.transactionFor, x.product_id, x.soldAmount , x.paidAmount , SUM(y.bal) balance FROM ( SELECT *,soldAmount-paidAmount bal FROM salestransaction WHERE salestransaction.soft_deleted ='No' AND salestransaction.customerId=2 ) x JOIN ( SELECT *,soldAmount-paidAmount bal FROM salestransaction WHERE  salestransaction.customerId=2 ) y ON y.id <= x.id LEFT join customers ON x.customerId=customers.id LEFT JOIN products ON x.product_id=products.id GROUP BY x.id";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function inndividualTransaction(){

        $sql = "SELECT x.id,x.transactionDate,x.customerId,customers.name, products.product_name,x.remarks,x.particulars,x.transactionFor, x.product_id, x.soldAmount , x.paidAmount , SUM(y.bal) balance FROM ( SELECT *,soldAmount-paidAmount bal FROM salestransaction WHERE salestransaction.soft_deleted ='No' AND salestransaction.customerId='$this->customer_id' ) x JOIN ( SELECT *,soldAmount-paidAmount bal FROM salestransaction WHERE salestransaction.customerId='$this->customer_id' ) y ON y.id <= x.id LEFT join customers ON x.customerId=customers.id LEFT JOIN products ON x.product_id=products.id where x.soft_deleted='No' AND x.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' GROUP BY x.id ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function prductsShip(){

        $sql = "SELECT * FROM ship WHERE categoryid=1 AND soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function prducts(){

        $sql = "SELECT * FROM products WHERE soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    /*Trial balance*/
    public function headTotal(){
        $sql = "";
        if($this->branch_id=='all'){$sql = "SELECT accounthead.headnameenglish, sum(salestransaction.amountOut) as debit,SUM(salestransaction.amountIn)as credit, SUM(salestransaction.amountIn)-SUM(salestransaction.amountOut) as balance FROM salestransaction LEFT JOIN accounthead on salestransaction.accountheadid=accounthead.id WHERE salestransaction.accountheadid='$this->account_head' AND salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND salestransaction.soft_deleted='No' ";}
        if($this->branch_id!='all'){
            $sql = "SELECT accounthead.headnameenglish, sum(salestransaction.amountOut) as debit,SUM(salestransaction.amountIn)as credit , SUM(salestransaction.amountIn)-SUM(salestransaction.amountOut) as balance FROM salestransaction LEFT JOIN accounthead on salestransaction.accountheadid=accounthead.id WHERE salestransaction.accountheadid='$this->account_head' AND branchid='$this->branch_id' AND salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND salestransaction.soft_deleted='No' ";
        }
        /*
        if($this->branch_id==2){
            $sql = "SELECT accounthead.headnameenglish, sum(salestransaction.amountOut) as debit,SUM(salestransaction.amountIn)as credit , SUM(salestransaction.amountIn)-SUM(salestransaction.amountOut) as balance FROM salestransaction LEFT JOIN accounthead on salestransaction.accountheadid=accounthead.id WHERE salestransaction.accountheadid='$this->account_head' AND branchid='$this->branch_id' AND salestransaction.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction' AND salestransaction.soft_deleted='No' ";
        }
        */
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function headWiseStatement(){
        ///echo $this->branch_id; echo $this->account_head;

        $sql = "SELECT X.id,X.transactionDate,accounthead.headnameenglish,accounthead.headnamebangla,bank.accountname, products.product_name, party.partyname, X.particulars, X.amountIn, X.amountOut,  SUM(Y.bal) balance, X.transactionType, X.voucherNo,X.crvoucher,X.challanno, X.transactionFor, X.receivedTo, X.receivedFrom, X.chequeNo, X.remarks FROM ( SELECT *, amountIn-amountOut bal FROM salestransaction ) X JOIN ( SELECT *, amountIn-amountOut bal FROM salestransaction ) Y ON Y.id <= X.id LEFT JOIN party ON X.customerId = party.id LEFT JOIN products ON X.product_id = products.id LEFT JOIN accounthead ON X.accountheadid=accounthead.id LEFT JOIN bank ON X.bankid=bank.id WHERE X.accountheadid='$this->account_head' AND  X.soft_deleted ='No' AND X.transactionDate BETWEEN '$this->from_Transaction' AND '$this->to_Transaction'  GROUP BY X.id ORDER BY X.transactionDate, X.voucherNo ASC,X.id;";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}