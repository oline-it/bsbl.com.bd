<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$allData = $objBookTitle->index();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$bankNme=$objTransaction->allbank();

$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


include_once ('header.php');
?>
<div class="content">
    <div class="container ctn">
        <div class="row">  <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?> </div>
        <div class="container"><br></div>
        <div class="row">
    <script type="text/javascript">
        function ChequeField(val){
            var element=document.getElementById('activeChequeField');
            if(val=='CASH CHEQUE'){
                element.style.display='block';
            }
            else{
                element.style.display='none';
            }
        }
    </script>
    <form class="form-group" name="vesselEntry" action="store.php" method="post">
        <input hidden name="addVessel" type="text" value="addVessel">
        <input name="modifiedDate"  type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">VESSEL NAME :</label> </div>
                    <div class="col-sm-4 text-left">
                        <input class="form-control " id="" name="vesselName" required type="text">
                    </div>
                        <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">IMPOTER NAME :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <input class="form-control " id="" name="importerName" required type="text">
                        </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">BANK NAME :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="bankId"  class="form-control text-uppercase ">
                            <?php
                            foreach ($bankNme as $singlBank){
                                echo  "<option class='text-uppercase' value='$singlBank->id'> $singlBank->bankname ($singlBank->accountname) </option>";}
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcNo"> LC NO :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control" name="lcNo" type="text" required>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="lcDate">  LC DATE :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control selectDate" name="lcDate" required  type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="ldt">LDT :</label></div>
                    <div class="col-sm-2 text-left">
                        <input class="form-control " id="" name="ldt" required type="text">
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarPrice">DOLLAR PRICE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="dollarPrice" type="number" required>
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="dollarRate">DOLLAR RATE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="dollarRate"  type="number" required>
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> WASTAGE :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="wastage"  id="wastage" required type="number">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="wastageValue"> WASTAGE VALUE  :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="wastageValue"  id="wastageValue" required type="number">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> REMARKS :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
</div>
    </form>

        </div>
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
