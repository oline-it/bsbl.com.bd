<?php
//ini_set('MAX_EXECUTION_TIME', -1);
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################
//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$allData = $objTransaction->statement();
$allClients=$objBookTitle->allClients();
$accountHead=$objTransaction->accounthead();
$allparticulars=$objBookTitle->allparticulars();

$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


include('header.php');
?>
<script type="text/javascript">

</script>
	<div class="content">
		<div class="container ctn">
            <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
                    <?php /* echo  'statement.php'; if($page=='/transaction.php' || $page=='/inndividualTransaction.php'){echo "inndividualTransaction.php";
}elseif($page=='/orders.php' || $page=='/allorders.php'){echo "allorders.php";} else{echo "statement.php";} */?>
					<form id="searchform" name="searchform" method="GET"   class="signleTranscation" >
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>From</td>
								<td>:</td>
								<td><input type="text" id="from-datepicker" class="form-control from-datepicker" autocomplete="off" name="fromTransaction" required></td>
							</tr>
							<tr>
								<td>To</td>
								<td>:</td>
								<td><input type="text" id="to-datepicker" class="form-control to-datepicker " autocomplete="off" name="toTransaction" required></td>
							</tr>
                            <tr>
                                <td>Branch</td>
                                <td>:</td>
                                <td><select  name="branchid" id="branchid" class="form-control" required>
                                        <option value='SELECT'>Select Branch</option>
                                        <?php
                                        if($singleUser->role=='admin'){
                                            echo  "<option value='all'>All Branch</option>
                                            <option value='1'>Head Office</option>
                                            <option value='2'>Yard</option>
                                            <option value='3'>Petty Cash (Yard)</option>";
                                        }else{echo  "<option value='2'>Yard</option><option value='3'>Petty Cash (Yard)</option>";}?>
                                    </select></td>
                            </tr>
                            <tr id="book">
                                <td>Book</td><td>:</td><td><select  name="bookname" id="bookname" class="form-control" required><option value='SELECT'>Select Book</option><option value='CASH'>Cash Book</option><option value='LEDGER'>General Leadger Book</option><option value='PARTY'>Party Leadger Book</option><option value='SALE'>Sales Leadger Book</option><option value='BANK'>Bank Book</option><option value='TBALANCE'>Trial Balance</option><option value='TACCOUNT'>Trading Account</option><option value='PLACCOUNT'>P/L Account</option><option value='BALANCESHEET'>Statement of Affairs</option> </select></td>
                            </tr>
                            <tr id="head" >
                            </tr>
                            <tr id="sale" >

                            </tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="Search" value="View">
                                    <input type="reset" class="btn btn-primary"  value="Reset">
                                </td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>

 <?php
 include ('footer.php');
 include ('footer_script.php');
?> 
 