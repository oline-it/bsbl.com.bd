<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Expense Head Wise Statement</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							
							<tr>
								<td>From</td>
								<td>:</td>
								<td><input type="date" class="form-control" name="from" required></td>
							</tr>
							<tr>
								<td>To</td>
								<td>:</td>
								<td><input type="date" class="form-control" name="to" required></td>
							</tr>
							<tr>
								<td>Expense Head</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="transactionFor" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="view" value="View Report"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 