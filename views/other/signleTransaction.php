<?php

include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 main">
					<form class="signleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Signle Transaction Entry</p>
								</div>
							</div>
						</div>
						<table class="table table-responsive" border="0">
							<tr>
								<td>Transaction Type</td>
								<td>:</td>
								<td>
									<select name="transType" class="form-control" required>
									  <option value="">None</option>
									  <option value="Payment">Payment</option>
									  <option value="Receipt">Receipt</option>
									  <option value="Journal">Journal</option>
									  <option value="Contra">Contra</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Transaction Date</td>
								<td>:</td>
								<td><input type="date" class="form-control" name="date" required></td>
							</tr>
							<tr>
								<td>Voucher No</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="voucherNo" required></td>
							</tr>
							<tr>
								<td>Transaction For</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="transactionFor" required></td>
							</tr>
							<tr>
								<td>Transaction Mode</td>
								<td>:</td>
								<td><select class="form-control" required>
									  <option value="Cash" selected>Cash</option>
									  <option value="Cash Cheque">Cash Cheque</option>
									  <option value="A/C Pay Cheque">A/C Pay Cheque</option>
									  <option value="Online Transfer">Online Transfer</option>
									  <option value="Pay Order">Pay Order</option>
									  <option value="ATM">ATM</option>
									  <option value="D.D">D.D</option>
									  <option value="T.T">T.T</option>
									  <option value="Others">Others</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Payment To</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="paymentTo" required></td>
							</tr>
							<tr>
								<td>Payment From</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="paymentFrom" required></td>
							</tr>
							<tr>
								<td>Cheque No</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="chequeNo" required></td>
							</tr>
							<tr>
								<td>Cheque Date</td>
								<td>:</td>
								<td><input type="date" class="form-control" name="chequeDate" required></td>
							</tr>
							<tr>
								<td>Remarks</td>
								<td>:</td>
								<td><textarea class="form-control" rows="3" required></textarea></td>
							</tr>
							<tr>
								<td>Amount</td>
								<td>:</td>
								<td><input type="number" class="form-control" name="amount" required></td>
							</tr>
							<tr>
								<td>In Words</td>
								<td>:</td>
								<td><input type="text" class="form-control" name="inWords" required></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" class="btn btn-primary" name="submit" value="Submit"></td>
							</tr>
						</table>
					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
<?php

 include('footer_script.php');
include('footer.php');
?> 
 