<?php

include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

//$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$allData = $objBookTitle->index();
$allClients=$objBookTitle->allClients();
$allparticulars=$objBookTitle->allparticulars();
$accountHead=$objTransaction->accounthead();
$bankNme=$objTransaction->allbank();
$msg = Message::getMessage();

if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;

$_SESSION['Page']= $page;
if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;
$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);
$serial = (  ($page-1) * $itemsPerPage ) +1;
if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBookTitle->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Multiple Voucher Entry</p>
								</div>
							</div>
						</div><hr/>
						
						
						
						<table class="table-responsive one">
							<tr>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher Type : 
										 <select class="form-control" required>
										  <option value="Payment" selected>Payment</option>
										  <option value="Receipt">Receipt</option>
										  <option value="Journal">Journal</option>
										  <option value="Contra">Contra</option>
										</select>
									</div>
								</td>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher Date :
										 <input type="date" name="voucherDate" class="form-control" required>
									</div>
								</td>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher No :
										 <input type="number" name="voucherNo" class="form-control" required>
									</div>
								</td>
							</tr>
						</table><br/>
						<table class="table table-responsive table-bordered tbtwo">
							<thead class="thead-light">
							<tr style="background-color:#4A3C8C;color:#FFFFFF;">
								<th>SL</th>
								<th>Payment To</th>
								<th>Payment From</th>
								<th>Branch Name</th>
								<th>Transaction Mode</th>
								<th>Cheque No</th>
								<th>Cheque Date</th>
								<th>Remarks</th>
								<th>Amount</th>
								<th></th>
							</tr>
							</thead>
							<tr>
								<td>1</td>
								<td><input type="text" class="form-control" name="paymentTo" required></td>
								<td><input type="text" class="form-control" name="paymentFrom" required></td>
								<td><input type="text" class="form-control" name="branchName" required></td>
								<td><select class="form-control" required>
									  <option value="Cash" selected>Cash</option>
									  <option value="Cash Cheque">Cash Cheque</option>
									  <option value="A/C Pay Cheque">A/C Pay Cheque</option>
									  <option value="Online Transfer">Online Transfer</option>
									  <option value="Pay Order">Pay Order</option>
									  <option value="ATM">ATM</option>
									  <option value="D.D">D.D</option>
									  <option value="T.T">T.T</option>
									  <option value="Others">Others</option>
									</select>
								</td>
								<td><input type="text" class="form-control" name="chequeNo" required></td>
								<td><input type="date" class="form-control" name="chequeDate" required></td>
								<td><input type="text" class="form-control" name="remarks" required></td>
								<td><input type="number" class="form-control" name="amount" required></td>
								<td>
									<a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
									<a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>
									<a href="#"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
								</td>
							</tr>
						</table>
						<div class="col-auto form-inline">
							In Words : 
							<input style="width:100%;" type="text" class="form-control" name="inWords" required>
						</div>
						<div class="col-auto form-inline">
							In Words (Total) : 
							<input style="width:100%;" type="text" class="form-control" name="inWordstotal" required>
						</div>
						<br/><div align="right"><input type="submit" class="btn btn-primary" name="submit" value="Submit"></div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
 <?php
 include('footer.php');
 include('footer_script.php');
?> 
 