<?php 
include('header.php');
?>
	<div class="content">
		<div class="container ctn">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 main">
					<form class="multipleTranscation">
						<div class="control">
							<div class="row">
								<div class="col-md-6">
									<a href="#" class="btn btn-secondary">EDIT</a>
									<a href="#" class="btn btn-secondary">Refresh</a>
								</div>
								<div class="col-md-6">
									<p class="nick text-right">Job Expenses</p>
								</div>
							</div>
						</div><hr/>
						
						
						
						<table class="table-responsive one">
							<tr>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher Type : 
										 <select class="form-control" required>
										  <option value="">Select Any</option>
										  <option value="Payment">Payment</option>
										  <option value="Receipt">Receipt</option>
										  <option value="Journal">Journal</option>
										  <option value="Contra">Contra</option>
										</select>
									</div>
								</td>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher Date :
										 <input type="date" name="voucherDate" class="form-control" required>
									</div>
								</td>
								<td style="padding-right:10px;">
									<div class="col-auto form-inline">
										Voucher No :
										 <input type="number" name="voucherNo" class="form-control" required>
									</div>
								</td>
							</tr>
						</table><br/>
						<table class="table table-responsive table-bordered tbtwo">
							<thead class="thead-light">
							<tr style="background-color:#4A3C8C;color:#FFFFFF;">
								<th>Serial</th>
								<th>Type</th>
								<th>From</th>
								<th>To</th>
								<th>Type</th>
								<th>Amount</th>
								<th></th>
							</tr>
							</thead>
							<tr>
								<td>1</td>
								<td><select name="typeA" class="form-control" required>
									  <option value="BDT" selected>BDT</option>
									  <option value="USD">USD</option>
									</select>
								</td>
								<td><input type="text" class="form-control" name="paymentFrom" required></td>
								<td><input type="text" class="form-control" name="paymentTo" required></td>
								<td><select name="typeB" class="form-control" required>
									  <option value="Percent" selected>Percent</option>
									  <option value="Amount">Amount</option>
									</select>
								</td>
								<td><input type="number" class="form-control" name="amount" required></td>
								<td>
									<a href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
									<a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>
									<a href="#"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
								</td>
							</tr>
						</table>
						<br/><div align="right"><input type="submit" class="btn btn-primary" name="submit" value="Submit"></div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
 <?php 
include('footer.php');
?> 
 