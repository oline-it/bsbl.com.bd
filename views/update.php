<?php
require_once("../vendor/autoload.php");
if(!isset($_SESSION) ) session_start();
//echo "<pre>".var_dump($_POST)."</pre>"; die();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
use \App\ExpenseIncome\ExpenseIncome;
use \App\Customer\Customer;
use \App\ExpenseIncome\Vessel;
use App\ExpenseIncome\Transaction;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(!$status) {     Utility::redirect('index.php');  return; }
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################
//echo "<pre>";var_dump($_POST); echo "</pre>"; die();

if(isset($_POST['updateTransaction'])) {
    //echo "<pre>"; var_dump($_POST);echo "</pre>"; die();
    $objTransaction=new ExpenseIncome();
    $objTransaction->setData($_POST);
    if($_POST['accheadId']=='24'){
        $objTransaction->updateTransaction();
        $objTransaction->updateSales();
    }
    else{
        $objTransaction->updateTransaction();
    }
}

if(isset($_POST['addVessel'])) {
    //var_dump($_POST); die();
    $objVessel=new Vessel();
    $objVessel->setData($_POST);
    $objVessel->store();
}