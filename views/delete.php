<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {

    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################

$msg = Message::getMessage();
############################## Message code ended #############################################


//if(isset($_POST['mark']) || isset($_SESSION['mark'])) {    // start of boss if
//if(isset($_GET['']) ) {    // start of boss if

############################### Delete code started ###########################################
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();

if(isset($_GET['YesButton'])) $objBookTitle->delete();

############################### Delete code ended ###########################################
include_once ('nav.php');
?>
<div class="container">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
<!-- required for search, block 4 of 5 start -->
<!-- Search nav start-->
<?php include_once ('searchnav.php'); ?>
<!-- Search nav ended-->

<form action="trashmultiple.php" method="post" id="multiple">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-header">
                    <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbarTwo" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="create.php "  class="btn btn-primary "  > Add new </a></li>
                        <li><a href="pdf.php" class="btn btn-primary "  >Download as PDF</a></li>
                        <li><a href="xl.php" class="btn btn-primary" " >Download as XL</a></li>
                        <li><a href="email.php?list=1" class="btn btn-primary" role="button" >Email </a></li>
                        <li><button type="button" class="btn btn-danger" id="delete">Delete  Selected</button></li>
                        <li><button type="submit" class="btn btn-warning">Trash Selected</button></li>
                        <li> <a href="trashed.php?Page=1"   class="btn btn-info" > View Trashed List</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
    <div class="container text-center " style="padding: 0 0 10px 0;" >
        <h1 style="text-align: center" ;">Are you sure ! You want to delete the following record?</h1>
    </div>
    <div class="container">
        <table  class="table table-striped table-bordered" cellspacing="0px">
            <tr style="background-color: #8aa6c1;">
                <th class="text-center">Select all<input id="select_all" type="checkbox" value="select all"></th>
                <th class="text-center">Serial</th>
                <th class="text-center">Customer</th>
                <th class="text-center">Product</th>
                <th class="text-center">Rate</th>
                <th class="text-center">Bandwidth</th>
                <th class="text-center">Start Date</th>
                <th class="text-center">End Date</th>
                <th class="text-center">Total Days</th>
                <th class="text-center">Amount</th>

            </tr>
            <?php
            //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
            echo "
                  <tr  style='background-color:'>
                     <td style='padding-left:40px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>$oneData->id</td>
                     <td style='text-align: center;'>$oneData->name</td>
                     <td class='text-center'>$oneData->product_name</td>
                     <td class='text-center'>$oneData->rate</td>
                     <td class='text-center'>$oneData->bandwidth mbps</td>
                      <td style='text-align: center;'>$oneData->fromDurationDate</td>
                     <td style='text-align: center;'>$oneData->toDurationDate</td>
                     <td class='text-center'>$oneData->totaldays</td>
                     <td class='text-center'>$oneData->amount Tk.</td>
                     
                  </tr>
              ";
            ?>
            <tr style='background-color:#BBB;'>
                <th class="text-right" colspan="9"> Total:</th>
                <th> </th>

            </tr>
            <tr>
                <td colspan="10" class="text-center">
                    <?php
                    echo "
                    <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>
                    <a href='index.php' class='btn btn-success'>No</a>";
                    ?>
                </td>
            </tr>
        </table>
    </div>
</form>

</div>

<?php
include ('footer.php');
include ('footer_script.php');
?>
