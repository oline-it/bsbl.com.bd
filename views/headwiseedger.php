<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('index.php');
    return;
}

############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('index.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
//var_dump($_GET); die();
################################ End of Session time calculation ##############################
$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction = new \App\ExpenseIncome\Transaction();
$allClients=$objBookTitle->allClients();
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
$allData =$objTransaction->setData($_GET);
//var_dump($_GET); die();
if ($singleUser->role=='admin'){
    if ($_GET['bookname']=='LEDGER' && ($_GET['branchid']=='1' || $_GET['branchid']=='2' ))
            $transactionData = $objTransaction->allsalesledger();
    if($_GET['bookname']=='LEDGER' && $_GET['branchid']=='all')
        $transactionData = $objTransaction->allsalesledger();
}else{
    if ($_GET['bookname'] == 'LEDGER' && $_GET['branchid'] == '1')
        $transactionData = $objTransaction->allsalesledger();
}

//var_dump($transactionData); die();
################## statement  block start ####################


    $_SESSION['someData']=$transactionData;
    //echo "<pre>"; var_dump($allData); echo "</pre>"; die();
    //Converting Object to an Array
    $objToArray = json_decode(json_encode($transactionData), True);
   //cho "<pre>"; var_dump($objToArray); echo "</pre>"; die();

    $customeName=$objToArray['0']['headnameenglish'];
    $sales="SALE";
    //$statementTotal=$objBookTitle->statementTotal();


    $serial = 1;

################## statement  block end ######################

include_once ('header.php');?>
<div class="content">
    <div class="container ctn">
 <div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<!-- required for search, block 4 of 5 start -->
<!-- Search nav start-->
<?php // include_once ('searchnav.php'); ?>
<!-- Search nav ended-->
<!-- Date selection ended -->
    <form action="trashmultiple.php" method="post" id="multiple">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="navbar-header">
                        <button style="background-color: #8aa6c1;" type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbarTwo" aria-expanded="false" aria-controls="navbarTwo">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!--
                    <div id="navbarTwo" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <a href="create.php "  class="btn btn-primary "  > Add new </a>
                            <a href="pdf.php" class="btn btn-primary "  >Download as PDF</a>
                            <a href="xl.php" class="btn btn-primary" " >Download as XL</a>
                            <a href="email.php?list=1" class="btn btn-primary" role="button" >Email </a>
                           <button type="button" class="btn btn-danger" id="delete">Delete  Selected</button>
                            <button type="submit" class="btn btn-warning">Trash Selected</button>
                            <a href="trashed.php?Page=1"   class="btn btn-info" > View Trashed List</a>
                        </ul>
                    </div>
                    -->
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <!-- <h1 style="text-align: center" ;">Book Title - Active List (<?php //echo count($allData) ?>)</h1>-->
        <div class="container text-center " style="padding: 0 0 5px 0;" >
            <h1> <?php
                /*if(($_REQUEST['customerId'])=='all') {
                echo "All Orders";} else{ echo $customeName;}
*/
                ?> </h1>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10 text-center">
                    <font style="font-size:25px; font-weight: bold;"  class="text-uppercase">Bhatiyari Ship Breakers Ltd.</font> <br>
                    <font style="font-size:15px">Bhatiyari, Sitakunda, Chittagong.</font><br>
                    ( <?php echo "Statement Since : ".$_GET['fromTransaction']." to ".$_GET['toTransaction'];?> )
                </div>
                <div class="col-sm-1"></div>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="text-left" style="font-size: large; font-weight: bold;text-align:;" >
                        <?php echo "HEAD: ".$customeName; ?>
                    </div>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4" style="padding-right: 35px;">
                    <div class="text-right" style="font-size: ; font-weight: ;" >
                        <?php echo "Print Date: ";  echo date('Y-m-d'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" align="center" style="padding-right: 35px;" >
                    <style>
                        table, td, th {
                            border: 1px solid black;
                            padding-left: 5px;
                            padding-right: 5px;    }
                        table {  border-collapse: collapse;   }
                        th {   height: 50px;  }
                    </style>
                      <table width="100%"   class=""  >
                        </tr>
                        <tr style="background-color:#F2F2F2;">
                            <th class="text-center">Select all<input id="select_all" type="checkbox" value="select all"></th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Date</th>
                           <th class="text-center" width="400px">Description</th>
                            <th class="text-center">Voucher <br> No</th>
                            <?php $weightHead="<th class=\"text-center\">Weight<br> (Kg)</th>";
                             if($customeName!=$sales){echo "";}else{echo $weightHead;}
                             ?>
                             <th class="text-center">DR<br> Payment <br> (Taka)</th>
                           <th class="text-center">CR<br>Received <br> (Taka)</th>
                            <th class="text-center"> Balance<br> (Taka)</th>
                        </tr>
                        <?php
                            $totalAmountIn=0;
                            $totalAmountOut=0;
                            $balance=0;
                            $totalWeight=0;

                        //  $serial= 1;  ##### We need to remove this line to work pagination correctly.
                       // echo "<pre>"; var_dump($allData); echo "</pre>"; die();

                        foreach($transactionData as $oneData){ ########### Traversing $someData is Required for pagination  #############
                            //$totalDays=abs('$oneData->toDurationDate','$oneData->fromDurationDate');
                            if($serial%2) $bgColor = "AZURE";
                            else $bgColor = "#ffffff";
                            $totalAmountIn=$totalAmountIn+$oneData->amountIn;
                            $totalAmountOut=$totalAmountOut+$oneData->amountOut;
                            //$totalAmount=$totalAmount+$oneData->amount ;
                            $balance=($balance-$oneData->amountOut)+$oneData->amountIn;

                            /*#####Total weight*/
                             $totalWeight=$totalWeight+($oneData->tonweight*1000)+$oneData->kgweight;
                            $voucherType="";
                            if($oneData->voucherNo!=0){$voucherType="DR -";}else{$voucherType="CR -";}
                            echo "
                  <tr  style='background-color: $bgColor'>
                     <td style='padding-left:40px;' class='text-center'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
                     <td style='text-align: center;'>$serial</td>
                      <td class='text-center'>$oneData->transactionDate</td>
                     <td class='text-left'>$oneData->product_name:$oneData->transactionFor $oneData->remarks </td>
                     <td class='text-center'>$voucherType $oneData->voucherNo</td>";
                            if($customeName!=$sales){echo "";}else{echo "<td class='text-right'>".number_format($oneData->tonweight)."-".number_format($oneData->kgweight)." </td>";}
                    echo "
                     <td style='text-align: right;'>".number_format($oneData->amountOut,0)."</td>
                     <td style='text-align: right;'> ".number_format($oneData->amountIn,0)."</td>
                     <td class='text-right'></td>
                     <!--
                     <td class='text-center'>
                       <div class='dropdown text center' style='background-color: inherit;'>
                      <button style='background-color: inherit;' class=' dropdown-toggle' type='button' id='menu1' data-toggle='dropdown'>Actions
                            <span class='caret'></span></button>
                        <ul style='background-color:#fcfcfc;' class='dropdown-menu' role='menu' aria-labelledby='menu1'>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='view.php?id=$oneData->id'>View</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='edit.php?id=$oneData->id'>Edit</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='trash.php?id=$oneData->id'>Trash</a></li>
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='delete.php?id=$oneData->id'>Delete</a></li>           
                            <li role='presentation'><a role='menuitem' tabindex=-1' href='email.php?id=$oneData->id'>Email</a></li>           
                        </ul>
                        </div>                        
                     </td>
                     -->
                  </tr>
              ";
                $serial++; }
                $term=""; $ledgerBalance=0;
                if($totalAmountIn==0) $ledgerBalance=$totalAmountOut; $term="DR"; if ($totalAmountIn!==0) $term="CR"; $ledgerBalance=$totalAmountIn;
                  echo "     
                        <tr style='background-color:; height: 50px; font-size: large; font-weight: bold;'>
                            <td class='text-right' colspan='5'> Total:</td>";
                       if($customeName!=$sales){ echo "";}else{echo "<td class='text-right'>".number_format($totalWeight,0)." Kg</td>";}
                        echo "
                            <td class='text-right'>".number_format($totalAmountOut,0)."</td>
                            <td class='text-right'>".number_format($totalAmountIn,0)."</td>
                            <td>$term ".number_format($ledgerBalance,0)."</td>
                        </tr>
                        "; ?>
                    </table>
                    <br>
                </div>

            </div>
            </div>
  </form>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->
    </div>
</div>
<?php
include ('footer.php');
include ('footer_script.php');
?>
